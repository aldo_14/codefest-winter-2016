package ir;

import static ir.IplImageTools.releaseImage;
import static org.bytedeco.javacpp.helper.opencv_core.CV_RGB;
import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U;
import static org.bytedeco.javacpp.opencv_core.cvClearMemStorage;
import static org.bytedeco.javacpp.opencv_core.cvGetSeqElem;
import static org.bytedeco.javacpp.opencv_core.cvPoint;
import static org.bytedeco.javacpp.opencv_imgproc.CV_AA;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.CV_FONT_ITALIC;
import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.cvInitFont;
import static org.bytedeco.javacpp.opencv_imgproc.cvPutText;
import static org.bytedeco.javacpp.opencv_imgproc.cvRectangle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_core.CvMemStorage;
import org.bytedeco.javacpp.opencv_core.CvRect;
import org.bytedeco.javacpp.opencv_core.CvScalar;
import org.bytedeco.javacpp.opencv_core.CvSeq;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.opencv_imgproc.CvFont;
import org.bytedeco.javacpp.opencv_objdetect;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.bytedeco.javacv.OpenCVFrameConverter.ToIplImage;
import org.bytedeco.javacv.OpenCVFrameGrabber;

import ir.haar.classifier.HaarClassifier;
import ir.haar.classifier.TrainingSetRecorder;

public class FacialRecognition {
	private final ToIplImage converter;
	private static final boolean WRITE_IMAGES = true;
	private static final double DRAWCONF = 50;

	private final FrameGrabber grabber;
	private final TrainingSetRecorder recorder;
	private final CvMemStorage storage;
	private final CvFont mCvFont;
	private final CanvasFrame frame;
	private final OpenCvFaceRecognizer fr;

	public FacialRecognition(final Optional<Integer> deviceNumber, final Optional<String> savedRecognizer) {
		Loader.load(opencv_objdetect.class);
		converter = new OpenCVFrameConverter.ToIplImage();
		grabber = new OpenCVFrameGrabber(deviceNumber.orElse(0));
		recorder = new TrainingSetRecorder(converter);
		storage = CvMemStorage.create();
		// init display stuff
		mCvFont = new CvFont();
		cvInitFont(mCvFont, CV_FONT_ITALIC, 1.0, 1.0, 0, 1, CV_AA);
		frame = new CanvasFrame("Camera " + deviceNumber.orElse(0), CanvasFrame.getDefaultGamma() / grabber.getGamma());
		fr = new OpenCvFaceRecognizer(savedRecognizer, storage);
	}

	public void startRecognition()
			throws org.bytedeco.javacv.FrameGrabber.Exception, IOException, InterruptedException {
		grabber.start();
		IplImage grabbedImage = converter.convert(grabber.grab());
		final IplImage grayImage = IplImage.create(grabbedImage.width(), grabbedImage.height(), IPL_DEPTH_8U, 1);
		while (frame.isVisible() && (grabbedImage = converter.convert(grabber.grab())) != null) {
			doRecognitionLoop(grabbedImage, grayImage);
			Thread.sleep(1000 / 15);
		}
		releaseImage(grabbedImage, grayImage);
		frame.dispose();
		grabber.stop();
	}

	public void doRecognitionLoop(IplImage grabbedImage, final IplImage grayImage)
			throws IOException, InterruptedException {
		cvClearMemStorage(storage);
		cvCvtColor(grabbedImage, grayImage, CV_BGR2GRAY);
		final CvSeq faces = findFacesIn(grabbedImage);
		if(faces.total() > 0) {
			if (WRITE_IMAGES) {
				recorder.recordTrainingSet(grabbedImage, faces);
			}
			//TODO use me
			final List<Identity> found = identifyAndDrawFacesInImage(grabbedImage, faces);
			writeSubjectTextOnScreen(grabbedImage, faces);
		}
		frame.showImage(converter.convert(grabbedImage));
	}

	public void writeSubjectTextOnScreen(final IplImage grabbedImage, final CvSeq faces) {
		cvPutText(grabbedImage, (faces.total() > 1 ? faces.total() + " SUBJECTS " : "SUBJECT ") + " DETECTED ", cvPoint(50, 50), mCvFont, CvScalar.RED);
	}

	public List<Identity> identifyAndDrawFacesInImage(IplImage grabbedImage, final CvSeq faces) {
		final List<Identity> faceIds = new ArrayList<>(faces.total());
		for (int i = 0; i < faces.total(); i++) {
			final BytePointer face = cvGetSeqElem(faces, i);
			final double[] result = fr.identifyFace(grabbedImage, face);
			final Identity personId = new Identity(extractNameFrom(fr.getId(new Double(result[0]).intValue())), result[1]);
			faceIds.add(personId);
			System.out.println("(" + i + ") " + personId.toString());
			createRectangleFromFace(grabbedImage, new CvRect(face), personId);
		}
		return faceIds;
	}

	public CvSeq findFacesIn(final IplImage grabbedImage) {
		return HaarClassifier.frontalface.applyTo(grabbedImage, storage);
	}

	private void createRectangleFromFace(final IplImage grabbedImage, final CvRect r, final Identity person) {
		final int x = r.x(), y = r.y(), w = r.width(), h = r.height();
		cvPutText(grabbedImage, person.toString(), cvPoint(r.x(), r.y() - 10), mCvFont, CvScalar.GREEN);
		cvRectangle(grabbedImage, cvPoint(x, y), cvPoint(x + w, y + h), CV_RGB(255.0D, 30.0D, 30.0D), 1, CV_AA, 0);
		r.close();
	}

	private static String extractNameFrom(final String name) {
		return name.toLowerCase().endsWith("-verified") ? name.split("-")[0].toUpperCase() : "DEVIANT";
	}
}