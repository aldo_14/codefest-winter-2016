package ir.haar.classifier;

import static org.bytedeco.javacpp.helper.opencv_objdetect.cvHaarDetectObjects;
import static org.bytedeco.javacpp.opencv_core.cvLoad;
import static org.bytedeco.javacpp.opencv_objdetect.CV_HAAR_DO_CANNY_PRUNING;

import java.io.File;
import java.net.URL;
import java.util.Optional;

import org.bytedeco.javacpp.opencv_core.CvMemStorage;
import org.bytedeco.javacpp.opencv_core.CvSeq;
import org.bytedeco.javacpp.opencv_objdetect.CvHaarClassifierCascade;
import org.bytedeco.javacpp.helper.opencv_core.CvArr;

public enum HaarClassifier {
	frontalfaceAlt(File.separatorChar + "haarcascade_frontalface_alt.xml"), 
	frontalfaceAlt2(File.separatorChar + "haarcascade_frontalface_alt2.xml"), 
	frontalfaceAltTree(File.separatorChar + "haarcascade_frontalface_alt_tree.xml"), 
	frontalface(File.separatorChar + "haarcascade_frontalface_default.xml");

	private final CvHaarClassifierCascade classifier;

	private HaarClassifier(final String source) {
		classifier = new CvHaarClassifierCascade(cvLoad(getFile(source).getAbsolutePath()));
		if (classifier.isNull()) {
			throw new RuntimeException("Error loading classifier file \"" + source + "\".");
		}
	}

	public File getFile(final String source) {
		final Optional<URL> resource = Optional.ofNullable(getClass().getResource(source));
		if(!resource.isPresent()) {
			throw new RuntimeException("Unable to load classifier " + source);
		}
		return new File(resource.get().getFile());
	}
	
	public CvSeq applyTo(final CvArr image, final CvMemStorage storage) {
		return cvHaarDetectObjects(image, classifier, storage, 1.2, 5, CV_HAAR_DO_CANNY_PRUNING);
	}
}
