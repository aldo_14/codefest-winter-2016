package ir.haar.classifier;

import static org.bytedeco.javacpp.opencv_core.cvGetSeqElem;

import java.io.File;
import java.io.IOException;

import org.bytedeco.javacpp.opencv_core.CvSeq;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacv.OpenCVFrameConverter.ToIplImage;

import ir.IplImageTools;
import static ir.IplImageTools.createImageFromFaceBox;

public class TrainingSetRecorder {

	private static long lastRecordedImage = 0;
	private static final int SCREENSHOT_RATE_LIMIT = 1000;
	private final IplImageTools imageUtils;
	private final String root = new File("").getAbsolutePath();
	

	public TrainingSetRecorder(final ToIplImage converter) {
		imageUtils = new IplImageTools(converter);
	}

	public void recordTrainingSet(final IplImage grabbedImage, final CvSeq faces) throws IOException {
		if (System.currentTimeMillis() - lastRecordedImage > SCREENSHOT_RATE_LIMIT) {
			if (faces.total() == 0) {
				writeNegativeFaceImage(grabbedImage);
			} else {
				writeImagesForEachFace(grabbedImage, faces);
			}
			lastRecordedImage = System.currentTimeMillis();
		}
	}

	private void writeNegativeFaceImage(IplImage grabbedImage) throws IOException {
		final File saveLoc = new File(root + "/training/negative/" + System.currentTimeMillis() + ".png");
		saveLoc.mkdirs();
		saveLoc.createNewFile();
		imageUtils.writeGreyscaleAndSizedFaceFile(saveLoc, grabbedImage);
	}

	private void writeImagesForEachFace(IplImage grabbedImage, final CvSeq faces) throws IOException {
		for (int i = 0; i < faces.total(); i++) {
			final File saveLoc = new File(root + "/training/faces/" + System.currentTimeMillis() + "_" + i + ".png");
			saveLoc.mkdirs();
			saveLoc.createNewFile();
			IplImage cropped = createImageFromFaceBox(grabbedImage, cvGetSeqElem(faces, i));
			imageUtils.writeGreyscaleAndSizedFaceFile(saveLoc, cropped);
		}
	}

}
