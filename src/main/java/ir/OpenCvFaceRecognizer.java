package ir;

import static ir.IplImageTools.FACE_X_SIZE;
import static ir.IplImageTools.FACE_Y_SIZE;
import static ir.IplImageTools.createImageFromFaceBox;
import static ir.IplImageTools.generateHistogram;
import static ir.IplImageTools.releaseImage;
import static ir.IplImageTools.releaseMat;
import static ir.IplImageTools.resizeImage;
import static org.bytedeco.javacpp.opencv_core.CV_32SC1;
import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U;
import static org.bytedeco.javacpp.opencv_core.cvGetSeqElem;
import static org.bytedeco.javacpp.opencv_face.createLBPHFaceRecognizer;
import static org.bytedeco.javacpp.opencv_imgcodecs.cvLoadImage;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.DoublePointer;
import org.bytedeco.javacpp.IntPointer;
import org.bytedeco.javacpp.opencv_core.CvMemStorage;
import org.bytedeco.javacpp.opencv_core.CvSeq;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.MatVector;
import org.bytedeco.javacpp.opencv_face.FaceRecognizer;

import ir.haar.classifier.HaarClassifier;

public class OpenCvFaceRecognizer {
	private final FaceRecognizer faceRecognizer;
	private final Map<Integer, String> idLabels;
	private final CvMemStorage storage;
	
	public OpenCvFaceRecognizer(final Optional<String> savedRecognizer, final CvMemStorage storage) {
		this.storage = storage;
		this.idLabels = new HashMap<>();
		setupLabels();
		if(savedRecognizer.isPresent() && new File(savedRecognizer.get()).exists()	) {
			this.faceRecognizer = createLBPHFaceRecognizer();
			this.faceRecognizer.load(savedRecognizer.get());
			System.out.println("Loaded recognizer from " + savedRecognizer.get());
		}
		else {
			faceRecognizer = this.generateAndTrainFaceRecognizer();
			faceRecognizer.save("test.recog");
		}	
	}

	public void setupLabels() {
		int i = 100;//labelAdd(100, "-verified", "alan", "juno", "poay");
		i = labelAdd(i+=100,  "-aberdeen-verified", "adrian", "alister", "ammelanby", "andrew", "barry", 
				"bfegan", "catherine", "david", "dhands", "dlow", "dpearson", "hin", "ian", "iroy", "jenni",
				"jim", "kay", "kirsty", "lisa", "louise", "marie", "martin", "michael", "mmanson", "nick",
				"pat", "paul", "peter", "simon", "stephen", "tock", "tracey", "trevor");
		labelAdd(i+=100,  "-verified", "alan", "KA", "KL", "KM", "KR", "MK", "NA", "NM", "TM", "UY", "YM");
	}
	
	private int labelAdd(int index, String suffix, String... labels) {
		for(final String label : labels) {
			System.out.println("Added " + index + ", " + label+suffix);
			this.idLabels.put(index, label+suffix);
			index+=100;
		}
		return index;
	}

	private FaceRecognizer generateAndTrainFaceRecognizer() {
		final FaceRecognizer recog = createLBPHFaceRecognizer();
		final List<IplImage> images = new LinkedList<>();
		final List<Integer> labels = new LinkedList<>();
		loadTrainingImages(images, labels);
		System.out.println("Loaded " + images.size() + " images");
		trainRecog(recog, images, labels);
		System.out.println("Trained all sets!");
		releaseImage(images.toArray(new IplImage[images.size()]));
		return recog;
	}
	
	public void loadTrainingImages(final List<IplImage> images, final List<Integer> labels) {
		for (final Integer index : idLabels.keySet()) {
			Optional<File[]> loaded = loadDataSet(idLabels.get(index));
			int counter = 1;
			for (final File f : loaded.orElse(new File[0])) {
				final IplImage grayResizedImage = IplImage.create(FACE_X_SIZE, FACE_Y_SIZE, IPL_DEPTH_8U, 1);
				System.out.println("Load " + f.getAbsolutePath());
				final IplImage loadedImage = getFaceImageFrom(f);
				cvCvtColor(resizeImage(FACE_X_SIZE, FACE_Y_SIZE, loadedImage), grayResizedImage, CV_BGR2GRAY);
				releaseImage(loadedImage);
				images.add(grayResizedImage);
				labels.add(index);
				System.out.println("Loaded file " + counter++ + " for index " + idLabels.get(index));
			}
		}
		System.out.println("Training with " + images.size() + " images");
	}

	public IplImage getFaceImageFrom(final File f) {
		final IplImage face = cvLoadImage(f.getAbsolutePath());
		final CvSeq facesFound = HaarClassifier.frontalface.applyTo(face, storage);
		return (facesFound.total() != 1) ? face : createImageFromFaceBox(face, cvGetSeqElem(facesFound, 0));
	}
	
	private Optional<File[]> loadDataSet(final String associateLabel) {
		final File dir = new File("training/" + associateLabel);
		System.out.println("Loading " + dir.getAbsolutePath());
		final File[] imageFiles = dir.listFiles(filter());
		System.out.println("Found " + (imageFiles == null ? "no" : imageFiles.length) + " image files");
		return Optional.ofNullable(imageFiles);
	}

	public FilenameFilter filter() {
		return new FilenameFilter() {
			public boolean accept(final File dir, final String name) {
				return name.toLowerCase().endsWith(".png") || name.toLowerCase().endsWith(".pgm")
				|| name.toLowerCase().endsWith(".jpg") || name.toLowerCase().endsWith(".tiff")  || name.toLowerCase().endsWith(".gif");
			}
		};
	}

	private FaceRecognizer trainRecog(final FaceRecognizer recog, final List<IplImage> trainingImg,
			final List<Integer> imgLabels) {
		final Mat labels = new Mat(trainingImg.size(), 1, CV_32SC1);
		final IntBuffer labelsBuf = labels.createBuffer();
		final MatVector images = new MatVector(trainingImg.size());

		// copy to bufs
		for (int i = 0; i < trainingImg.size(); i++) {
			final IplImage currentImage = trainingImg.get(i);
			final int currentLabel = imgLabels.get(i);
			labelsBuf.put(i, currentLabel);
			images.put(i, new Mat(currentImage));
		}

		final long start = System.currentTimeMillis();
		System.out.println("Start training");
		final TimerThread recogCount = new TimerThread("Training: ");
		recogCount.start();
		recog.train(images, labels);
		recogCount.stopRunning();
		System.out.println("Completed training after " + (System.currentTimeMillis() - start) + "ms");
		releaseMat(labels);
		releaseImage(trainingImg.toArray(new IplImage[trainingImg.size()]));
		return recog;
	}
	

	double[] identifyFace(final IplImage grabbedImage, final BytePointer face) {
		final IplImage image = createImageFromFaceBox(grabbedImage, face);
		final IplImage resizedFace = resizeImage(FACE_X_SIZE, FACE_Y_SIZE, image);
		final IplImage faceRecogGsImage = IplImage.create(FACE_X_SIZE, FACE_Y_SIZE, IPL_DEPTH_8U, 1);
		
		//TODO; gamme correction, difference of guassian filtering (illumination normalization steps)
		//http://opencv-users.1802565.n2.nabble.com/Illumination-Normalization-td6550856.html
		
		//generate grayscale image
		cvCvtColor(resizedFace, faceRecogGsImage, CV_BGR2GRAY);
		
		//gamma correct face
		
		//DOG (difference of gaussian filtering) to normalize illumination
		
		//generate histogram
		final IplImage finalImage = generateHistogram(faceRecogGsImage);
		System.out.println("Get UMap from image");
		final Mat extractedFace = new Mat(finalImage);
		final IntPointer prediction = new IntPointer(1);
		final DoublePointer confidence = new DoublePointer(1);
		faceRecognizer.predict(extractedFace, prediction, confidence);
		System.out.println("Prediction: " + prediction.get(0) + ":" + confidence.get(0));
		releaseImage(resizedFace, faceRecogGsImage, finalImage);
		releaseMat(extractedFace);
		return new double[] { prediction.get(0), confidence.get(0) };
	}

	public String getId(int out) {
		return idLabels.containsKey(out) ? idLabels.get(out) : "UNKNOWN";
	}
}
