package ir;

import static javax.imageio.ImageIO.write;
import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U;
import static org.bytedeco.javacpp.opencv_core.cvCopy;
import static org.bytedeco.javacpp.opencv_core.cvCreateImage;
import static org.bytedeco.javacpp.opencv_core.cvGetSize;
import static org.bytedeco.javacpp.opencv_core.cvSetImageROI;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.cvResize;
import static org.bytedeco.javacpp.opencv_imgproc.equalizeHist;

import java.io.File;
import java.io.IOException;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.opencv_core.CvRect;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameConverter.ToIplImage;

public class IplImageTools {
	public static final int FACE_X_SIZE = 256;
	public static final int FACE_Y_SIZE = FACE_X_SIZE;
	private final ToIplImage converter;
	private final Java2DFrameConverter paintConverter;
	
	public IplImageTools(final ToIplImage converter) {
		this.converter = converter;
		this.paintConverter = new Java2DFrameConverter();
	}
	
	public void writeGreyscaleAndSizedFaceFile(final File saveLoc, final IplImage image) throws IOException {
    	final IplImage resizeImage = resizeImage(FACE_X_SIZE, FACE_Y_SIZE, image);
		final IplImage faceRecogGsImage = IplImage.create(FACE_X_SIZE, FACE_Y_SIZE, IPL_DEPTH_8U, 1);
		cvCvtColor(resizeImage, faceRecogGsImage, CV_BGR2GRAY); 
		write(paintConverter.getBufferedImage(converter.convert(faceRecogGsImage), 1), "png", saveLoc);
		releaseImage(faceRecogGsImage, resizeImage);
	}
	
	public static IplImage resizeImage(int xSize, int ySize, final IplImage loadedImage) {
		final IplImage resizeImage = IplImage.create(xSize, ySize, loadedImage.depth(), loadedImage.nChannels());
		cvResize(loadedImage,resizeImage);
		return resizeImage;
	}

	public static IplImage createImageFromFaceBox(IplImage grabbedImage, final BytePointer face) {
		final IplImage copiedSource = cvCreateImage(cvGetSize(grabbedImage), grabbedImage.depth(), grabbedImage.nChannels());
		cvCopy(grabbedImage, copiedSource);
		cvSetImageROI(copiedSource, new CvRect(face));
		final IplImage cropped = cvCreateImage(cvGetSize(copiedSource), copiedSource.depth(), copiedSource.nChannels());
		cvCopy(copiedSource, cropped);
		releaseImage(copiedSource);
		return cropped;
	}
	
	public static IplImage generateHistogram(final IplImage grayResizedImage) {
		final Mat histogrammed = new Mat(IplImage.create(FACE_X_SIZE, FACE_Y_SIZE, IPL_DEPTH_8U, 1));
		equalizeHist(new Mat(grayResizedImage), histogrammed);
		return new IplImage(histogrammed);
	}

	public static void releaseImage(final IplImage...images) {
		for(final IplImage image: images) {
			image.release();
		}
	}

	public static void releaseMat(final Mat...images) {
		for(final Mat image: images) {
			image.release();
		}
	}
}
