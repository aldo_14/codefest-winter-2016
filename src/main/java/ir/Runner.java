package ir;

import java.util.Optional;

public class Runner {
	public static void main(final String[] args) throws Exception {
		new FacialRecognition(Optional.of(0), Optional.of("test.recog")).startRecognition();
	//	new FacialRecognition(Optional.of(0), Optional.empty()).startRecognition();
	}
}
